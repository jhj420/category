package handler

import (
	"category/common"
	"category/domain/model"
	"category/domain/service"
	"category/proto/category"
	"context"
	"github.com/prometheus/common/log"
)
type Category struct{
     CategoryDataService service.ICategoryDataService
}
//创建分类的服务
func (c *Category) CreateCategory(ctx context.Context, request *category.CategoryRequest, response *category.CreateCategoryResponse) error {
	category := &model.Category{}
	//request-->category
	err := common.SwapTo(request,category)
	if err != nil{
		return err
	}
	categoryId,err := c.CategoryDataService.AddCategory(category)
	if err != nil{
		return err
	}
	response.Message = "分类添加成功"
	response.CategoryId = categoryId
	return  nil
}
//更新分类的服务
func (c *Category) UpdateCategory(ctx context.Context, request *category.CategoryRequest, response *category.UpdateCategoryResponse) error {
	category := &model.Category{}
	//request-->category
	err := common.SwapTo(request,category)
	if err != nil{
		return err
	}
	err = c.CategoryDataService.UpdateCategory(category)
	if err != nil{
		return err
	}
	response.Message = "分类更新成功"
	return  nil
}
//删除分类服务
func (c *Category) DeleteCategory(ctx context.Context, request *category.DeleteCategoryRequest, response *category.DeleteCategoryResponse) error {
	err := c.CategoryDataService.DeleteCategory(request.CategoryId)
	if err != nil{
		return err
	}
	response.Message = "删除成功"
	return nil
}
//根据名称查找分类
func (c *Category) FindCategoryByName(ctx context.Context, request *category.FindByNameRequest,response *category.CategoryResponse) error {
	category,err := c.CategoryDataService.FindCategoryByName(request.CategoryName)
	if err != nil{
		return err
	}
	return common.SwapTo(category,response)
}
//根据id查找分类
func (c *Category) FindCategoryByID(ctx context.Context, request *category.FindByIdRequest, response *category.CategoryResponse) error{
	category,err := c.CategoryDataService.FindCategoryByID(request.CategoryId)
	if err != nil{
		return err
	}
	return common.SwapTo(category,response)
}
//根据级别查找分类
func (c *Category) FindCategoryByLevel(ctx context.Context, request *category.FindByLevelRequest, response *category.FindAllResponse) error {
	categorySlice,err := c.CategoryDataService.FindCategoryByLevel(request.Level)
	if err != nil{
		return err
	}
	categorySliceToResponse(categorySlice,response)
	return nil
}
//根据parent查找分类
func (c *Category) FindCategoryByParent(ctx context.Context, request *category.FindByParentRequest, response *category.FindAllResponse) error {
	categorySlice,err := c.CategoryDataService.FindCategoryByParent(request.ParentId)
	if err !=nil {
		return err
	}
	categorySliceToResponse(categorySlice,response)
	return nil
}
//查找所有分类
func (c *Category) FindAllCategory(ctx context.Context, request *category.FindAllRequest, response *category.FindAllResponse) error {
	categorySlice,err := c.CategoryDataService.FindAllCategory()
	if err !=nil {
		return err
	}
	categorySliceToResponse(categorySlice,response)
	return nil
}

//categorySlice-->Response
func categorySliceToResponse(categorySlice []model.Category,response *category.FindAllResponse)  {
	for _,cg := range categorySlice{
		cr := &category.CategoryResponse{}
		err := common.SwapTo(cg,cr)
		if err != nil{
			log.Error(err)
			break
		}
		response.Category = append(response.Category, cr)
	}
}
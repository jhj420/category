package common

import "encoding/json"

//通过 json tag进行结构体转换
func SwapTo(request,category interface{}) error {
	dataByte,err := json.Marshal(request)
	if err != nil{
		return err
	}
	return json.Unmarshal(dataByte,category)
}
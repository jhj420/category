

.PHONY: proto
proto:
	protoc --go_out=. --micro_out=. ./proto/user/user.proto

.PHONY: build
build: 

	go build -o category *.go

.PHONY: test
test:
	go test -v ./... -cover

.PHONY: docker
docker:
	docker build . -t category:latest
